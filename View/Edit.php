
<?php

        session_start(); 

$level = $_SESSION['level'];
	
$username = $_SESSION['username'];

//Pindah ke Form Login Jika belum Login
if(empty($_SESSION['username'])){
	header('location:Index.php');
}
else {
$id = $_GET['edit'];
?>

<!DOCTYPE HTML>
<html>

	<!-----HEADER------>

<head>
	<link href="../Config/Template.css" type="text/css" rel="stylesheet">
	<link href="../Config/Dropdown.css" type="text/css" rel="stylesheet">
	<link href="../Config/Tamu.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" type="text/css">
	<title>Form Edit Pdf</title>

</head>
<body>
		<div class="header">
		<table class="header">
		
		<tr>
			<td>
				<img class="logo" src="Images/logo-baznas.png">
			</td>
			
			<td>
				<h1>ONLINE LIBRARY</h1>
			</td>
			
			<td class="search">
			
			<form action="Library.php">
				<input class="search" type="text" placeholder="Search Pdf.." name="search">
				<input class="searchbutton" type="button" value="Search">	
			</form>	
				
			</td>
			
			</tr>
		
		</table>
		</div>
		
		<div class="menu-wrap">
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a></li>
				
		<li class="Drop2"><a>Admin <i class="fa fa-angle-down"></i></a>
			<ul>
				<li><a href="Upload.php">Upload PDF</a></li>
				<li><a href="Signup.php">Add User</a></li>
				
			</ul>
		</li>
	
		<li class="Drop"><a><img class="Drop" src="Images/Dropdown.png"></a>
			<ul>
				<li><a ><?php echo $username; ?></a></li>
				<li><a href="Index.Php?Logout=true">Log Out</a></li>
			</ul>
		</li>
		
	</ul>
		</div>
		
	<!-----CLOSE HEADER------>	
	
	
	
			<!-----BODY------->	
	<div class="chest">
		<div class="form">
	<?php
	
	//Menghadirkan Edit-controller,php
	
	include "../Controller/Edit-controller.php";
	
	//Memanggil fungsi data($ss)
	$data = data($id);

?>
	<h3>File Edit Form</h3>
	
	<br><br>


	<form method="POST" action="../Controller/Edit-controller.php">
	
	<table class="form" class="upload" style="font-family:calibri">
	
	<input type="hidden" name="fileedit" value="<?php echo $data['nama_file'];  ?>">
	
	<input type="hidden" name="id" value="<?php echo $id;  ?>">
	
	<tr><th>Title</th> <td><input type="text" placeholder="&nbsp;Title..." name="newname">&nbsp; .PDF</td></tr>
	
	<tr><th>Type</th> <td>
	
	<select name="tipe">
	
	<?php if($data['tipe']=='Peraturan & Pedoman'){
		?>
		<option value="Informasi Sertifikasi" selected="true">Informasi Sertifikasi</option>
		
		<option value="Peraturan & Pedoman">Peraturan & Pedoman</option>
		<?php
	}
		else{
			?>
			<option value="Informasi Sertifikasi">Informasi Sertifikasi</option>
		
			<option value="Peraturan & Pedoman" selected="true">Peraturan & Pedoman</option>
			<?php
		}
		
	?>
	</select>
	</td></tr>
	
	<tr><th>Description</th> <td><textarea name="description" rows=6 cols=40><?php echo $data['deskripsi']; ?></textarea></td></tr>
	
	<tr><td ><input type="submit" value="Edit" name="update"></td><td ><input type="reset" value="Reset" name="Reset"></td></tr>
	</table>
	<p class="note">*Kosongkan title jika tidak ingin mengubah title<p>
	</form>
	
	</div>
		</div>
			<!-----CLOSE BODY------>
			
			
	<!-----Footer------>
	<div class="footer">
	<div class="footer2">
	<p class="copyright">Copyright  &copy; <?php echo date('Y'); ?> Online - Library by Asharisan. Alrights Reserved.
	</p>
	</div>
	<ul>
		
		<li><a href="Home.php">Home</a></li>
		<li><a href="Library.php">Library</a>
			
		</li>
		<li><a href="#">Go Top</a>
			
		</li>
		
		
	</ul>
	</div>
</html>

	
	<?php

}

?>